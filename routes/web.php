<?php

use App\User;
use App\Employee;
use App\Refundation;
use App\Salary;
use App\Plan;
use App\AdvancePayment;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



// Route::get('/readusers', function () {
    
//     // povlaci sve usere iz baze i cuva ih u promenjivu $users
//     $users = User::all();
//     // vrati sve podatke iz promenjive $users
//     return $users;

//     // foreach($users as $user){
//     //     // return $user->username;
//     //     return $user;
//     // }
// });

// Route::get('/readuser', function () {
    
//     // povlaci user-a iz baze ciji je $id=2
//     $user = User::where('id', 2)->take(1)->get();
//     // vrati sve podatke o tom useru
//     return $user;

// });

// Route::get('/createuser', function () {
    
//     $user = new User;
    
//     $user->username = "marko";
//     $user->email = "marko2@gmail.com";
//     $user->password = "123";
   
//     $user->save();

// });

// Route::get('/create', function () {
    
//     User::create(['username'=>'anja', 'email'=>'anja@gmail.com', 'password'=>'123']);

// });



// Route::get('/updateuser', function () {
    
//     $user = User::find(7);
    
//     $user->username = "janko";
//     $user->email = "janko@gmail.com";
//     $user->password = "123";
   
//     $user->save();

// });

// Route::get('/update', function () {
    
//     User::where('id', 2)->where('username', 'maja')->update(['username'=>'maja2', 'email'=>'maja2@gmail.com']);

// });


// Route::get('/delete', function () {
    
//     $user = User::find(2);

//     $user->delete();

// });

// Route::get('/destroy', function () {
    
//     User::destroy(7);

// });




/*
|------------------------------------------------
| ELOQUENT Relationships
|------------------------------------------------
*/


// ONE TO ONE


// // Return employee with user_id=1
// Route::get('/user/{id}/employee', function ($id) {
    
//     return User::find($id)->employee;
// });


// // Insert employee for user with user_id=1
// Route::get('/insert', function () {
    
//     $user = User::findOrFail(1);

//     $employee = new Employee(['first_name'=>'nikola', 
//                               'last_name'=>'nikolic', 
//                               'address'=>'Romanovih 33', 
//                               'phone_number'=>'123123',
//                               'professional_qualifications'=>'profesor']);

//     $user->employee()->save($employee);
// });

// Update employee with user_id=1
// Route::get('/update', function () {
    
//     $employee = Employee::whereUserId(1)->first();

//     $employee->first_name = "Nikola";

//     $employee->save();
// });


// Read employee with user_id=1
// Route::get('/read', function () {
    
//     $user = User::findOrFail(1);

//     echo $user->employee;

//     // Samo ime empolyee-a
//     // echo $user->employee->first_name;
// });

// Delete employee with user_id=1
// Route::get('/delete', function () {
    
//     $user = User::findOrFail(1);
//     $user->employee()->delete();
    
//     return "Done";
// });




// ONE TO MANY


//user have many posts
// EMPLOYEES HAVE MANY REFUNDATIONS

// Create refundation where employee_id=1
// Route::get('/create-refundation', function () {
    
//     $employee = Employee::findOrFail(1);

//     $refundation = new Refundation(['amount'=> '35.56', 
//                                     'paid_at' => '2020-02-19 13:48:46',
//                                     'created_at'=> '2020-02-17 16:47:41', 
//                                     'updated_at'=> '2020-02-18 13:48:46', 
//                                     'status'=> 'placeno'
//                                     ]);

//     $employee->refundations()->save($refundation);
// });


// Read refundation where employee_id=1
// Route::get('/read-refundation', function () {
    
//     $employee = Employee::findOrFail(1);

//     // return $employee->refundations;

//     foreach($employee->refundations as $refundation) {

//         echo $refundation->amount."\n";
//         echo $refundation->paid_at."\n";
//         echo $refundation->created_at."\n";
//         echo $refundation->updated_at."\n";
//         echo $refundation->status."\n\n";
//     }
// });


// Update refundation where employee_id=1
// Route::get('/update-refundation', function () {
    
//     $employee = Employee::find(1);

//     $employee->refundations()->whereId(1)->update(['amount' => "45.87"]);
    
// });


// Delete refundation where employee_id=1
// Route::get('/delete-refundation', function () {
    
//     $employee = Employee::findOrFail(1);

//     $employee->refundations()->whereId(1)->delete();    
    
// });




// EMPLOYEES HAVE MANY SALARIES

// Create salary where employee_id=1
// Route::get('/create-salary', function () {
    
//     $employee = Employee::findOrFail(1);
//     $salary = new Salary(['amount'=> '350.56', 
//                         'paid_at' => '2020-02-19 13:48:46',
//                         'created_at'=> '2020-02-17 16:47:41', 
//                         'updated_at'=> '2020-02-18 13:48:46', 
//                         'status'=> 'placeno'

//     ]);
//     $employee->salaries()->save($salary);
// });


// Read salary where employee_id=1
// Route::get('/read-salary', function () {
    
//     $employee = Employee::findOrFail(1);

//     // return $employee->salaries;

//     foreach($employee->salaries as $salary) {

//         echo $salary->amount."\n";
//         echo $salary->paid_at."\n";
//         echo $salary->created_at."\n";
//         echo $salary->updated_at."\n";
//         echo $salary->status."\n\n";
//     }
// });


// // Update salary where employee_id=1
// Route::get('/update-salary', function () {
    
//     $employee = Employee::find(1);
//     $employee->salaries()->whereId(1)->update(['amount' => "450.87"]);
    
// });


// // Delete salary where employee_id=1
// Route::get('/delete-salary', function () {
    
//     $employee = Employee::findOrFail(1);

//     $employee->salaries()->whereId(1)->delete();     
// });



// // EMPLOYEES HAVE MANY PLANS

// // Create plan where employee_id=1
// Route::get('/create-plan', function () {
    
//     $employee = Employee::findOrFail(1);
//     $plan = new Plan(['amount'=> '350.56', 
//                     'created_at'=> '2020-02-17 16:47:41', 
//                     'updated_at'=> '2020-02-18 13:48:46', 
//                     'status'=> 'placeno'
//     ]);
//     $employee->plans()->save($plan);
// });


// // Read plan where employee_id=1
// Route::get('/read-plan', function () {
    
//     $employee = Employee::findOrFail(1);

//     // return $employee->plans;

//     foreach($employee->plans as $plan) {

//         echo $plan->amount."\n";
//         echo $plan->paid_at."\n";
//         echo $plan->created_at."\n";
//         echo $plan->updated_at."\n";
//         echo $plan->status."\n\n";
//     }
// });


// // Update plan where employee_id=1
// Route::get('/update-plan', function () {
    
//     $employee = Employee::find(1);
//     $employee->plans()->whereId(1)->update(['amount' => "550.87"]);
    
// });


// // Delete plan where employee_id=1
// Route::get('/delete-plan', function () {
    
// // if( globa(user)->id != 1) {
// //     return false;
// // }

//     $employee = Employee::findOrFail(1);
//     $employee->plans()->whereId(1)->delete();     
// });


// // EMPLOYEES HAVE MANY Advance Payment

// // Create advance payment where employee_id=1
// Route::get('/create-advance-payment', function () {
    
//     $employee = Employee::findOrFail(1);
//     $advance_payment = new AdvancePayment(['amount'=> '50.56', 
//                                         'paid_at' => '2020-02-19 13:48:46',
//                                         'created_at'=> '2020-02-17 16:47:41', 
//                                         'updated_at'=> '2020-02-18 13:48:46', 
//                                         'status'=> 'placeno'
//     ]);
//     $employee->advancepayment()->save($advance_payment);
// });
