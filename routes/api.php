<?php

use Illuminate\Http\Request;

use App\User;
use App\Employee;
use App\Refundation;
use App\Salary;
use App\Plan;
use App\AdvancePayment;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Route employee create, read, read all, update and delete
 */ 
Route::post('/employees/create', 'EmployeesController@store');
Route::get('/employees/get', 'EmployeesController@get');
Route::get('/employees/index', 'EmployeesController@index');
Route::put('/employees/update/{id}', 'EmployeesController@update');
Route::delete('/employees/destroy/{id}', 'EmployeesController@destroy');


/**
 * Route create, read, read all, update and delete
 * For admin user
 */ 
Route::group(['middleware' => ['user.check', 'token']], function() {
    Route::post('/users/create', 'UsersController@store');
    Route::get('/users/get', 'UsersController@get');
    Route::get('/users/index', 'UsersController@index');
    Route::put('/users/update/{id}', 'UsersController@update');
    Route::delete('/users/destroy/{id}', 'UsersController@destroy');

    Route::get('/refundations/index', 'RefundationsController@index');
    Route::delete('/refundations/destroy/{id}', 'RefundationsController@destroy');

    Route::post('/salaries/create/{id}', 'SalariesController@store');
    Route::get('/salaries/index', 'SalariesController@index');
    Route::put('/salaries/update/{id}', 'SalariesController@update');
    Route::delete('/salaries/destroy/{id}', 'SalariesController@destroy');

    Route::post('/plans/create/{id}', 'PlansController@store');
    Route::get('/plans/index', 'PlansController@index');
    Route::put('/plans/update/{id}', 'PlansController@update');
    Route::delete('/plans/destroy/{id}', 'PlansController@destroy');

    Route::get('/advance_payments/index', 'AdvancePaymentController@index');
    Route::put('/advance_payments/update/{id}', 'AdvancePaymentController@update');
    Route::delete('/advance_payments/destroy/{id}', 'AdvancePaymentController@destroy');

    Route::get('/reports/get', 'ReportsController@get');
    Route::get('/reports/index', 'ReportsController@index');
  });

  
/**
 * Route create, read, read all, update and delete
 * For admin user and for other role users
 */ 
Route::group(['middleware' => ['user.check', 'role', 'token']], function() {

    Route::get('/refundations/get', 'RefundationsController@get');
    Route::post('/refundations/create/{id}', 'RefundationsController@store');
    Route::put('/refundations/update/{id}', 'RefundationsController@update');

    Route::get('/salaries/get', 'SalariesController@get');

    Route::get('/plans/get', 'PlansController@get');

    Route::get('/advance_payments/get', 'AdvancePaymentController@get');
    Route::post('/advance_payments/create/{id}', 'AdvancePaymentController@store');
    // Route::put('/advance_payments/update/{id}', 'AdvancePaymentController@update');
  });

  // Rout for login middleware
  Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    // Route::post('refresh', 'AuthController@refresh');
    // Route::post('me', 'AuthController@me');
});