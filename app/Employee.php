<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 
        'last_name',
        'address', 
        'phone_number', 
        'professional_qualifications',
        'user_id'
    ];

    public function refundations(){

        return $this->hasMany('App\Refundation');

    }

    public function salaries(){

        return $this->hasMany('App\Salary');

    }

    public function plans(){

        return $this->hasMany('App\Plan');

    }

    public function advancepayments(){

        return $this->hasMany('App\AdvancePayment');

    }

}
