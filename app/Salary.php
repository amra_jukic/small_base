<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount', 
        'paid_at',
        'created_at', 
        'updated_at', 
        'status'
    ];
}
