<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Employee;

class EmployeesController extends Controller
{
   
   /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $employee = User::find($request->id)->employee;
        return response()->json($employee);
    }
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Employee::all();
        return response()->json($employee);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::findOrFail(1);

        $validated = $this->validate($request, [
            'first_name'=> 'required|max:10',
            'last_name'=> 'required|max:15',
            'address' => 'required',
            'phone_number'=> 'required|min:8',
            'professional_qualifications' => 'required|max:50',                 
        ]);

        $user->employee()->create($validated);
        return response()->json($validated);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::whereUserId($id)->first();
        
        $validated = $this->validate($request, [
            'first_name'=> 'required|max:10',
            'last_name'=> 'required|max:15'                
        ]);

        $employee = $employee->update($validated);
        return response()->json($employee);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->employee()->delete();
        
        return "Done";
    }
}
