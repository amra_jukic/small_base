<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Salary;
use App\Employee;
use App\Http\Requests\StoreSalaryRequest;
use App\Http\Requests\UpdateSalaryRequest;
use App\Http\Resources\SalaryResource as SalaryResource;

class SalariesController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $salary = Employee::find($request->id)->salaries;
        // return response()->json($salary);
        return SalaryResource::collection($salary);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $salary = Salary::all();
        // return response()->json($salary);
        return SalaryResource::collection(Salary::paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSalaryRequest $request, $id)
    {
        $employee = Employee::findOrFail($id);        
        $employee = $employee->salaries()->create(['amount'=> $request->amount,
                                        'paid_at'=> $request->paid_at,
                                        'created_at' => $request->created_at,
                                        'updated_at'=> $request->updated_at,
                                        'status' => $request->status,                 
                                       ]);
        // return response()->json($employee); 
        return new SalaryResource($employee);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSalaryRequest $request, $id)
    {
        $salary = Salary::whereId($id)->first();
        $salary->update(['amount'=> $request->amount,
                         'paid_at'=> $request->paid_at,
                         'created_at' => $request->created_at,
                         'updated_at'=> $request->updated_at,
                         'status' => $request->status,                 
                        ]);
        // return response()->json($salary);
        return new SalaryResource($salary);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $salary = Salary::destroy($id);
        return response()->json($salary);
    }
}
