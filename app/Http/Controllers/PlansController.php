<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;
use App\Employee;
use App\Http\Requests\StorePlanRequest;
use App\Http\Requests\UpdatePlanRequest;
use App\Http\Resources\PlansResource as PlansResource;

class PlansController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $plan = Employee::find($request->id)->plans;
        // return response()->json($plan);
        return PlansResource::collection($plan);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $plan = Plan::all();
        // return response()->json($plan);
        return PlansResource::collection(Plan::paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePlanRequest $request, $id)
    {
        $employee = Employee::findOrFail($id);
        $employee->plans()->create(['amount'=> $request->amount,
                                    'paid_at'=> $request->paid_at,
                                    'created_at' => $request->created_at,
                                    'updated_at'=> $request->updated_at,
                                    'status' => $request->status,  
                                   ]);
        // return response()->json($request);
        return new PlansResource($request); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePlanRequest $request, $id)
    {
        $plan = Plan::whereId($id)->first();
        $plan->update([ 'amount' => $request->amount,                            
                        'paid_at' => $request->paid_at,
                        'created_at' => $request->created_at,
                        'updated_at' => $request->updated_at,
                        'status' => $request->status,
                      ]);
        // return response()->json($plan);
        return new PlansResource($plan);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $plan = Plan::destroy($id);
        return response()->json($plan);
    }
}
