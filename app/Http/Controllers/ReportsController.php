<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Employee;
use App\Salary;
use App\Refundation;
use App\AdvancePayment;
use App\Plan;
use App\Validator;

class ReportsController extends Controller
{
    public function get(Request $request)
    {
        if($request->od && $request->od){

        $employee_salary = Salary::where([['employee_id', '=',$request->id],
                                          ['created_at', '>=', date($request->od)],
                                          ['updated_at', '<=', date($request->do)]
                                         ])->first();

        $employee_refundation = Refundation::where([['employee_id', '=',$request->id],
                                                    ['created_at', '>=', date($request->od)],
                                                    ['updated_at', '<=', date($request->do)]
                                                   ])->first();

        $employee_plan = Plan::where([['employee_id', '=',$request->id],
                                      ['created_at', '>=', date($request->od)],
                                      ['updated_at', '<=', date($request->do)]
                                     ])->first();


        $emp_advance_payment = AdvancePayment::where([['employee_id', '=',$request->id],
                                                     ['created_at', '>=', date($request->od)],
                                                     ['updated_at', '<=', date($request->do)]
                                                    ])->first();

        $collection = collect([$employee_salary, 
                              $employee_refundation, 
                              $employee_plan, 
                              $emp_advance_payment]);

        return response()->json($collection);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->od && $request->od){

            $employee_salary = Salary::where([['created_at', '>=', date($request->od)],
                                              ['updated_at', '<=', date($request->do)]
                                             ])->get();
    
            $employee_refundation = Refundation::where([['created_at', '>=', date($request->od)],
                                                        ['updated_at', '<=', date($request->do)]
                                                       ])->get();
    
            $employee_plan = Plan::where([['created_at', '>=', date($request->od)],
                                          ['updated_at', '<=', date($request->do)]
                                         ])->get();
    
    
            $emp_advance_payment = AdvancePayment::where([['created_at', '>=', date($request->od)],
                                                         ['updated_at', '<=', date($request->do)]
                                                        ])->get();
    
            $collection = collect([$employee_salary, 
                                  $employee_refundation, 
                                  $employee_plan, 
                                  $emp_advance_payment]);
    
            return response()->json($collection);
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
