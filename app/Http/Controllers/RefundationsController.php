<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Refundation;
use App\Http\Requests\StoreRefundationRequest;
use App\Http\Requests\UpdateRefundationRequest;
use App\Http\Resources\RefundationResource as RefundationResource;

class RefundationsController extends Controller
{
     /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $refundation = Employee::find($request->id)->refundations;
        // return response()->json($refundation);

        return RefundationResource::collection($refundation);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $refundation = Refundation::all();
        // return response()->json($refundation);
        return RefundationResource::collection(Refundation::paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRefundationRequest $request, $id)
    {
        $employee = Employee::findOrFail($id);
        $employee = $employee->refundations()->create(['amount'=> $request->amount,
                                            'paid_at'=> $request->paid_at,
                                            'created_at' => $request->created_at,
                                            'updated_at'=> $request->updated_at,
                                            'status' => $request->status,  
                                            ]);
        // return response()->json($request);
        return new RefundationResource($employee);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRefundationRequest $request, $id)
    {
        $refundation = Refundation::whereId($id)->first();
        $refundation->update([ 'amount' => $request->amount,                            
                               'paid_at' => $request->paid_at,
                               'created_at' => $request->created_at,
                               'updated_at' => $request->updated_at,
                               'status' => $request->status,
      ]);        
        // return response()->json($refundation);
        return new RefundationResource($refundation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $refundation = Refundation::destroy($id);
        return response()->json($refundation);
    }
}
