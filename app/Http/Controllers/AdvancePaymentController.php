<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AdvancePayment;
use App\Employee;
use App\Http\Requests\StoreAdvancePaymentRequest;
use App\Http\Requests\UpdateAdvancePaymentRequest;
use App\Http\Resources\AdvancePaymentResource as AdvancePaymentResource;


class AdvancePaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        $advance_payment = Employee::find($request->id)->advancepayments;
        // return response()->json($advance_payment);
        return AdvancePaymentResource::collection($advance_payment);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $advance_payment = AdvancePayment::all();
        // return response()->json($advance_payment);
        return AdvancePaymentResource::collection(AdvancePayment::paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAdvancePaymentRequest $request,$id)
    {        
        $employee = Employee::findOrFail($id);
        $employee->advancepayments()->create(['amount'=> $request->amount,
                                              'paid_at'=> $request->paid_at,
                                              'created_at' => $request->created_at,
                                              'updated_at'=> $request->updated_at,
                                              'status' => $request->status,  
                                            ]);
        // return response()->json($request);
        return new AdvancePaymentResource($request); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdvancePaymentRequest $request, $id)
    {
        $advance_payment = AdvancePayment::whereId($id)->first();    
        $advance_payment->update(['amount' => $request->amount,                            
                                  'paid_at' => $request->paid_at,
                                  'created_at' => $request->created_at,
                                  'updated_at' => $request->updated_at,
                                  'status' => $request->status,
                                ]);         
        // return response()->json($advance_payment);
        return new AdvancePaymentResource($advance_payment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


     
    public function destroy($id)
    {
        $advance_payment = AdvancePayment::destroy($id);
        return response()->json($advance_payment);
    }
}
