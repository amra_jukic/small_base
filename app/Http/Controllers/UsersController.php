<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Employee;
use App\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserResource as UserResource;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function get(Request $request)
    {
        //  $user = User::where('id', $request->id)->take(1)->get();
        //  return response()->json($user);
        return new UserResource(User::find(1));
    }

    /**
     * Display a listing of the resource.
     *daco 
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $users = User::all();
        // return response()->json($users);
        return UserResource::collection(User::paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $request->password = Hash::make($request->password);
        $user = User::create([ 'username' => $request->username,                            
                               'email' => $request->email,
                               'password' => $request->password
                            ]);
        try {
            $employee = Employee::create([ 'user_id' => $user->id,                            
                                           'first_name' => $request->employee['first_name'],
                                           'last_name' => $request->employee['last_name'],
                                           'address' => $request->employee['address'],
                                           'phone_number' => $request->employee['phone_number'],
                                           'professional_qualifications' => $request->employee['professional_qualifications']
                                        ]);
            
        }catch (\Exception $e) {
            return response()->json($e);
        }
        // return response()->json($user);
        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::where('id', $id)->update([ 'username' => $request->username,                            
                                                 'email' => $request->email,
                                                 'password' => $request->password,
                                               ]);
        try {
            $employee = Employee::where('id', $id)->update(['first_name' => $request->employee['first_name'],
                                                            'last_name' => $request->employee['last_name'],
                                                            'address' => $request->employee['address'],
                                                            'phone_number' => $request->employee['phone_number'],
                                                            'professional_qualifications' => $request->employee['professional_qualifications']
                                                            ]);
            
        }catch (\Exception $e) {
            return response()->json($e);
        }
        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::destroy($id);
        return response()->json($user);
    }
}
