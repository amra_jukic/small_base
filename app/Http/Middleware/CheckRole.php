<?php

namespace App\Http\Middleware;

use Closure;
use App\Salary;
use App\Refundation;
use App\Plan;
use App\AdvancePayment;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){

    $emp = Salary::where('employee_id', $request->id)->first();

        if ($emp) {
            
            // user object
            return $next($request);
            }else {
                return response()->json("false rola");
            }

    $emp_refundation = Refundation::where('employee_id', $request->id)->first();

        if ($emp_refundation) {
            
            return $next($request);
            }else {
                return response()->json("false rola");
            }

    $emp_plan = Plan::where('employee_id', $request->id)->first();

        if ($emp_plan) {
                
            // user object
            return $next($request);
            }else {
                return response()->json("false rola");
            }
                

    $emp_advance_payment = AdvancePayment::where('employee_id', $request->id)->first();

        if ($emp_advance_payment) {
                    
            // user object
            return $next($request);
            }else {
                return response()->json("false rola");
            }            
    }
}