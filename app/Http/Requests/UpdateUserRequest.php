<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:15|regex:/(^(([a-z]+)(\d)*)+$)/u',
            'email' => 'required|regex:/^.+@.+$/i',
            'password'=> 'required|min:8',
            'employee.first_name' => 'required|regex:/(^([A-Z]{1}[a-z]+)$)/u',
            'employee.last_name' => 'required|regex:/(^([A-Z]{1}[a-z]+)$)/u',
            'employee.address' => 'required|regex:/(^([A-Z]{1}[a-z]+)( \d+)?$)/u',
            'employee.phone_number' => 'required|regex:/(^(\d){8,10}$)/u',
            'employee.professional_qualifications' => 'required|regex:/(^([A-Z]{1}[a-z]+)$)/u',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => 'A username is required',
            'email.required' => 'A email is required',
            'password.required' => 'A password is required',
            'employee.first_name.required' => 'A first name is required',
            'employee.last_name.required' => 'A last name is required',
            'employee.address.required' => 'A address is required',
            'employee.phone_number.required' => 'A phone number is required',
            'employee.professional_qualifications.required' => 'A professional qualifications is required',
        ];
    }
}
