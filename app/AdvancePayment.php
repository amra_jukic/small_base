<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvancePayment extends Model
{
    // protected $table = 'advancepayments';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'amount', 
        'paid_at',
        'created_at', 
        'updated_at', 
        'status'
    ];

}
